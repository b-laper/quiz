/// <reference types="react-scripts" />
declare module "*.mp3" {
  const src: string;
  const autoplay: boolean;
  export default src;
}
