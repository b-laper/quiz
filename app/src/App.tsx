import axios from "axios";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AllQuizzes } from "./components/allQuizzes";
import { Auth } from "./components/auth";
import { CreateQuiz } from "./components/createQuiz";
import { Dashboard } from "./components/dashboard";
import { MyQuizzes } from "./components/myQuizzes";
import { setUser } from "./store/actions";

const App = (): JSX.Element => {
  const dispatch = useDispatch();

  useEffect(() => {
    const localStorageID = localStorage.getItem("_ID");
    if (localStorageID) {
      axios
        .get(`/api/users/${localStorageID}`)
        .then((res) => {
          dispatch(setUser(res.data.user));
        })
        .catch((err) => console.error(err));
    }
  }, [dispatch]);

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Auth />} />
          <Route path="/my-quizzes" element={<MyQuizzes />} />
          <Route path="/all-quizzes" element={<AllQuizzes />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/create-quiz" element={<CreateQuiz />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
