import "./toast.css";

interface IToastProps {
  message: string;
  visibile: boolean;
  bgc: string;
}

export const Toast = ({ message, visibile, bgc }: IToastProps) => {
  return visibile ? (
    <div style={{ backgroundColor: `${bgc}` }} className="toast">
      {message}
    </div>
  ) : (
    <span></span>
  );
};
