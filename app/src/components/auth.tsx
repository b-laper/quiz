import "./auth.css";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { SignIn } from "./signin";
import { SignUp } from "./signup";
import { Toast } from "./toast";
import logo from "../img/db2.png";
import dbzlogo from "../img/dbz head.png";
import axios from "axios";
import { authUser } from "../store/actions";
import { useNavigate } from "react-router-dom";

export const Auth = (): JSX.Element => {
  const [tab, setTab] = useState("signin");
  const [showToast, setShowToast] = useState<boolean>(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const signIn = ({ email, password }: { email: string; password: string }) => {
    axios
      .post("/api/users/login", { email, password })
      .then((res) => {
        if (res.data.success) {
          dispatch(
            authUser({
              _id: res.data.user._id,
              user: res.data.user,
              token: res.data.token,
            })
          );
          navigate("/dashboard");
        }
        if (res.data.token !== undefined) {
          console.log("Authorized");
        } else {
          setShowToast(true);
          setTimeout(() => setShowToast(false), 3000);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };
  const changeTab = () => {
    if (tab === "signup") setTab("signin");
    else setTab("signup");
    console.log(tab);
  };

  const signUp = ({
    firstName,
    lastName,
    email,
    password,
  }: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
  }) => {
    axios
      .post("/api/users/register", { email, password, firstName, lastName })
      .then((res) => {
        console.log(res.data, email, password, firstName, lastName);
        if (res.data.success) {
          setTab("signin");
        }
      });
  };

  let page =
    tab === "signin" ? <SignIn signIn={signIn} /> : <SignUp signUp={signUp} />;

  return (
    <section>
      <div className="auth-wrapper">
        <div className="left">
          <img className="left-logo" src={logo} alt="left-logo" />
        </div>
        <div className="right">
          <header>
            <img className="header" src={dbzlogo} alt="" />
          </header>
          <Toast
            bgc="red"
            visibile={showToast}
            message="Invalid email or password"
          />
          {page}
          <div className="new" onClick={changeTab}>
            {tab === "signin"
              ? "New to quiz, Please sign up here!"
              : "Already have an account? - Sign in here!"}
          </div>
        </div>
      </div>
    </section>
  );
};
