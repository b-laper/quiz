import { useState } from "react";

interface ISignUpProps {
  signUp: ({
    email,
    password,
    firstName,
    lastName,
  }: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
  }) => void;
}

export const SignUp = ({ signUp }: ISignUpProps): JSX.Element => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");

  return (
    <section className="sign-in-wrapper">
      <div className="form">
        <div className="input-wrapper">
          <input
            className="input"
            type="text"
            placeholder="First Name"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </div>
        <div className="input-wrapper">
          <input
            className="input"
            type="text"
            placeholder="Last Name"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />

          <div className="input-wrapper">
            <input
              className="input"
              type="text"
              placeholder="Email Address"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="input-wrapper">
            <input
              className="input"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        <button
          onClick={() => signUp({ firstName, lastName, email, password })}
          className="btn"
        >
          Sign Up
        </button>
      </div>
    </section>
  );
};
