import "./createQuiz.css";
import axios from "axios";
import { useEffect, useState } from "react";
import Sidebar from "./sidebar";
import { useNavigate } from "react-router-dom";
import { Modal } from "./modal";
import closeIcon from "../img/icon-close.png";
import { Toast } from "./toast";

export const CreateQuiz = (): JSX.Element => {
  const closeAnswer = (e: any, idx: number): void => {
    if (e.target.localName === "img") {
      let newArr: JSX.Element[] = Object.assign([], inputsArr);
      let newInputs: string[] = Object.assign([], inputValues);
      newArr.splice(idx, 1);
      newInputs.splice(idx, 1);
      setInputArr(newArr);
      setInputValues(newInputs);
      console.log(inputsArr, inputValues);
    }
  };

  const inputJsx = (index: number) => (
    <>
      <input
        onChange={(event) => setCorrectAnswer(event.currentTarget.value)}
        type="radio"
        className="radio"
        name="answer"
        value={index}
      />
      <input
        onChange={(event) => {
          const newInputValues = [...inputValues];
          newInputValues[index] = event.target.value;
          setInputValues(newInputValues);
        }}
        value={inputValues[index]}
        type="text"
        placeholder="Edit answer"
      />
      <span className="close-icon">
        <img src={closeIcon} alt="close-icon" />
      </span>
    </>
  );
  const [questions, setQuestions] = useState<object[]>([]);
  const [priv, setPriv] = useState<boolean>(false);
  const [correctAnswer, setCorrectAnswer] = useState<string>();
  const [toggleModal, setToggleModal] = useState<boolean>(true);
  const [inputsArr, setInputArr] = useState<JSX.Element[]>([]);
  const [quizName, setQuizName] = useState<string>("");
  const [questionName, setQuestionName] = useState<string>("");
  const [inputValues, setInputValues] = useState<string[]>([]);
  const [imgUrl, setImgUrl] = useState<string>(
    "https://cdn.vox-cdn.com/thumbor/m9jZBbdvpl8wqN4mKCo7StPebms=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/23331741/0db75b0953c009c4032b6f266615f5ba.jpg"
  );
  const [toast, setToast] = useState<boolean>(false);
  const toggleModalWindow = () => {
    setToggleModal(!toggleModal);
  };
  const navigate = useNavigate();

  useEffect(() => {
    console.log(questions);
    questions.map((item) => console.log(item));

    if (!localStorage.getItem("JWT_PAYLOAD")) {
      navigate("/");
    }
  }, [questions]);

  const addAnswer = (index: number): void => {
    setInputArr([...inputsArr, inputJsx(index)]);
  };

  const saveQuestion = (): void => {
    const question = {
      questionName,
      answers: inputValues,
      correctAnswer,
      quizName,
      priv,
    };

    setQuestions([...questions, question]);
    console.log(questions, inputValues);
  };
  const saveQuiz = () => {
    const question = {
      questionName,
      correctAnswer,
      answers: inputValues,
    };

    console.log(question);
    axios
      .post("/api/quizzes/create", {
        quizName,
        question,
        priv,
        createdBy: localStorage.getItem("_ID"),
        imgUrl,
      })
      .then((res) => {
        if (res.data.success) {
          setToast(true);
          setTimeout(() => {
            setToast(false);
          }, 2000);
        }
      });
  };

  const createQuizJSX = (
    <div className="modal-window">
      <h1>Create quiz</h1>
      <label htmlFor="quiz-name">Quiz name:</label>
      <input
        onChange={(e) => setQuizName(e.target.value)}
        value={quizName}
        type="text"
        placeholder="Enter quiz name"
      />
      <br />
      <label htmlFor="quiz-name">Make it private:</label>
      <input
        onChange={(e) => setPriv(e.target.checked)}
        type="checkbox"
        className="checkbox"
      />
      <br />
      <label>Img url: </label>
      <input
        onChange={(e) => setImgUrl(e.target.value)}
        type="text"
        value={imgUrl}
      />
      <button onClick={toggleModalWindow} className="btn bottom">
        Create quiz
      </button>
    </div>
  );
  const quizDataJSX = (
    <div className="modal-window">
      <button onClick={saveQuiz}>Save quiz</button>
      <h1>Enter question</h1>

      <label htmlFor="question">No.{questions.length + 1}: </label>
      <input
        value={questionName}
        onChange={(e) => setQuestionName(e.target.value)}
        type="text"
        placeholder="Type your quiz question"
      />
      <br />
      {inputsArr.map((inputGroup, index) => (
        <div
          className="answer relative"
          key={index}
          onClick={(e) => closeAnswer(e, index)}
        >
          {inputGroup}
        </div>
      ))}
      <div className="button-group">
        <button onClick={() => addAnswer(inputsArr.length)} className="btn ">
          Add answer
        </button>
        <button onClick={saveQuestion} className="btn save ">
          Save question
        </button>
      </div>
    </div>
  );

  return (
    <div className="create-quiz-wrapper">
      <Toast bgc="green" message="Quiz created" visibile={toast} />
      <div>
        <Sidebar />
      </div>

      <div className="content">
        {toggleModal ? (
          <Modal JSX={createQuizJSX} />
        ) : (
          <Modal JSX={quizDataJSX} />
        )}
      </div>
    </div>
  );
};
