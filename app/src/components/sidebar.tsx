import "./sidebar.css";
import avatar from "../img/frieza-avatar.png";
import { useTypedSelector } from "../hooks/use-typed-selector";
import { Link } from "react-router-dom";

export const Sidebar = (): JSX.Element => {
  const user = useTypedSelector((state) => state.users);
  if (user._id !== "") {
    return (
      <section className="sidebar-wrapper">
        <div className="header">Quiz</div>
        <div className="user">
          <div className="avatar">
            <img className="avatar" src={avatar} alt="" />
          </div>
          <div className="name">{`${user.firstName} ${user.lastName}`}</div>
        </div>
        <nav>
          <div className="links">
            <Link className="link" to={"/dashboard"}>
              Dashboard
            </Link>
            <Link className="link" to={"/account"}>
              Account
            </Link>{" "}
            <Link className="link" to={"/create-quiz"}>
              Create new quiz
            </Link>
            <Link className="link" to={"/my-quizzes"}>
              My quizes
            </Link>
            <Link className="link" to={"/all-quizzes"}>
              All quizzes
            </Link>
          </div>
        </nav>
      </section>
    );
  } else {
    return <div>Loading...</div>;
  }
};

export default Sidebar;
