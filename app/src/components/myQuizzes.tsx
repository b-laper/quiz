import "./MyQuizzes.css";
import axios from "axios";
import { useEffect, useState } from "react";
import Sidebar from "./sidebar";

export const MyQuizzes = () => {
  const [quizzes, setQuizzes] = useState<any>([]);

  useEffect(() => {
    axios
      .get(`/api/quizzes/my-quizzes/${localStorage.getItem("_ID")}`)
      .then((res) => {
        setQuizzes(res.data);
      });

    console.log(quizzes);
  }, [quizzes]);

  return (
    <div className="all-quizzes-wrapper">
      <div>
        <Sidebar />
      </div>
      <div className="content">
        <header className="header-top">My quizzes</header>
        <div className="quizzes-wrapper">
          {quizzes.map((quiz: any, idx: number) => (
            <div key={`quiz${idx}`} className="quiz-card card">
              <img src={quiz.imgUrl} alt="quiz-image" />
              <div className="quiz-name">{quiz.name}</div>
              <div className="questions">{quiz.questions.length} Questions</div>
              <div className="take-quiz btn">Start quiz</div>

              <section>
                <div className="top-section">
                  <div className="views">{quiz.views} </div>
                  <div className="likes">{quiz.likes} </div>
                </div>
              </section>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
