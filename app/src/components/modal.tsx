import "./modal.css";
interface IModalProps {
  JSX: JSX.Element | JSX.Element[];
}

export const Modal = ({ JSX }: IModalProps) => {
  return <div>{JSX}</div>;
};
