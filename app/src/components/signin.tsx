import { useState } from "react";

interface ISignInProps {
  signIn: ({ email, password }: { email: string; password: string }) => void;
}

export const SignIn = ({ signIn }: ISignInProps): JSX.Element => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  return (
    <section className="sign-in-wrapper">
      <div className="form">
        <div className="input-wrapper">
          <input
            className="input"
            type="text"
            placeholder="Email Address"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="input-wrapper">
          <input
            className="input"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button onClick={() => signIn({ email, password })} className="btn">
          Sign In
        </button>
      </div>
    </section>
  );
};
