import { createReducer } from "@reduxjs/toolkit";
import { authUser, setUser } from "../actions";

export interface IUser {
  createdAt: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  __v: number;
  _id: string;
}
const initialState: IUser = {
  createdAt: "",
  email: "",
  firstName: "",
  lastName: "",
  password: "",
  __v: 0,
  _id: "",
};

export const usersReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(authUser, (state, { payload }) => {
      if (window.localStorage) {
        localStorage.setItem("JWT_PAYLOAD", payload.token);
        localStorage.setItem("_ID", payload._id);
        state = payload.user;
      } else {
        alert("local Storage not supported");
      }
      return state;
    })
    .addCase(setUser, (state, { payload }) => {
      state = payload;
      return state;
    });
});
