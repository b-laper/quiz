export enum usersActionTypesEnum {
  AUTH = "USERS/AUTH",
  SET_USER = "USERS/SET_USER",
}
