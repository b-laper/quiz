import { createAction } from "@reduxjs/toolkit";
import { IUser } from "../reducers";
import { usersActionTypesEnum } from "./action-types-enum";

export interface IUserPayload {
  token: string;
  _id: string;
  user: IUser;
}

export const authUser = createAction(
  usersActionTypesEnum.AUTH,
  ({ token, _id, user }: IUserPayload) => {
    return {
      payload: { token, _id, user },
    };
  }
);

export const setUser = createAction(
  usersActionTypesEnum.SET_USER,
  (payload: IUser) => {
    return {
      payload,
    };
  }
);
