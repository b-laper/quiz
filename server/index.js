const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const userRoutes = require("./routes/users");
const quizRoutes = require("./routes/quizzes");

require("dotenv").config();

const app = express();
const PORT = process.env.PORT || 9000;
let server;

//middleware config

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true, limit: "20mb" }));
app.use(bodyParser.json({ limit: "15mb" }));

app.use("/api/users", userRoutes);
app.use("/api/quizzes", quizRoutes);

mongoose
  .connect(process.env.DB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => console.log("Database connected"))
  .catch((err) => console.log(err, "Data base connection failed"));

server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
