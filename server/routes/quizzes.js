const express = require("express");
const Quiz = require("../models/quiz");
const checkAuth = require("../middleware/check-auth");
const router = express.Router();

router.post("/create", checkAuth, (req, res) => {
  let quiz = new Quiz({
    ...req.body.quiz,
    createdBy: req.body.createdBy,
  });
  quiz.save().then((result) => {
    res.status(200).json({ success: true });
  });
});

router.get("my-quizzes/:id", checkAuth, (req, res) => {
  Quiz.find({ createdBy: req.params.id }).then((result) => {
    res.status(200).json(result);
  });
});

router.get("/all-quizzes", checkAuth, (req, res) => {
  Quiz.find().then((result) => {
    res.status(200).json(result);
  });
});

module.exports = router;
