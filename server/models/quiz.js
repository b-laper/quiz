const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QuizSchema = new Schema({
  quizName: {
    type: String,
    required: false,
  },
  priv: {
    type: Boolean,
    required: false,
    default: false,
  },
  quiz: [
    {
      type: Object,
      contains: {
        questionName: {
          type: String,
          required: true,
        },

        correctAnswer: {
          type: String,
          required: true,
        },
        answers: {
          type: Array,
          required: true,
        },
      },
    },
  ],
  createdAt: {
    type: Date,
    default: new Date(),
  },
  deleted: {
    type: Boolean,
    default: false,
  },
  imgUrl: {
    type: String,
    required: false,
  },
  views: {
    type: Number,
    default: 0,
  },
  likes: {
    type: Number,
    default: 0,
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  scores: {
    type: Array,
    default: [],
  },
});

module.exports = User = mongoose.model("Quiz", QuizSchema);
